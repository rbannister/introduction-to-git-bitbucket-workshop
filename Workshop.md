# Introduction to Git & Bitbucket

The source for this workshop is available as a Git repository at [bitbucket.org/jacksonj04/introduction-to-git-bitbucket-workshop](https://bitbucket.org/jacksonj04/introduction-to-git-bitbucket-workshop).

As you go through this workshop you will find it useful to write down your observations as to how you would improve it, either for yourself or others, since you will be asked to contribute some changes back to this document.

## Introducing Git

### Getting Git

* Download Git for your operating system (if it's not already installed) from [git-scm.com](http://git-scm.com/).
* Install Git using the downloaded installer. Accept the default options.

### Getting Your Environment Ready

#### Windows

* Create the folder you want to use for your code repository.
* Right click the folder and choose "Git Bash" from the menu.

#### Everyone Else

* Create the folder you want to use for your code repository.
* Open up Terminal, and `cd` to the directory you wish to use for your code repository.

### Creating & Initial Commit

* Create your first repository by running the `git init` command.
* Create or copy some files into your repository folder --- referred to from now on as your "working directory". This can be anything from a simple 'hello world' script through to an entire piece of code.
* Run the `git add .` command to stage the files ready for committing to your repository.
* You should now enter your name and email address. Please make sure to use your lincoln.ac.uk address in order to allow for your commits to be properly attributed later on. The commands are 'git config --global user.email "studentid@lincoln.ac.uk"' and 'git config --global user.name "John Smith"'. The purpose of the '--global' tag is so that you will automatically be attributed to commits throughout all of your Git sessions. Removing this would only attribute you to the repository you are currently working on.
* Run `git commit -a -m 'First commit.'` to commit your changes to your repository.
* Type `git status`.

If everything has gone to plan you should see

	# On branch master
	nothing to commit, working directory clean

The command `git status` is useful at all points in development, since it shows you files you've not yet added and changes which haven't yet been committed.

### Make Some Changes

* Alter one or two of your files, or add and remove files. Don't forget that if you're adding new files you will need to run `git add .` again.
* Run `git commit -a -m` to commit your changes, leaving a suitable message after the `-m`.

You can -- and should -- repeat the `git add` and `git commit` steps whenever you feel your code has reached a point worth recording. It is not unusual in software development companies to commit hundreds of times a day, and with Git there is very little overhead in terms of time or storage to committing, so do it often!

* Make two or three more changes and commits to your code.

If you want to see a complete list of your commits you can do this with the `git log` command.

### Time Travelling (or; Returning to a Previous Commit)

One thing you will want to do with Git is return to a previous commit, most commonly to return to a prior copy of working code. Git actually has three ways of doing this, depending on what you're trying to achieve.

#### Checkout

If you want to just look at an old copy of code you can use the `git checkout` command to return to a specific commit. Run `git log` to see a list of commit identifiers (a string of letters and numbers) along with times and commit messages. Find the identifier of the commit you want, and then run `git checkout [identifier]`, for example for identifier "0766c053..." the command would be `git checkout 0766c053...`.

* Run the `git checkout` command using your first commit.

Running this command will change the state of your working directory to match how it was at the time of the commit.  From here you can can look around to see how things were in the past and make experimental commits, but your changes will *not* be kept permanently once you run another `git checkout`. If you want to, you can use the `-b` flag when you run `checkout` to create a new branch of your code and keep subsequent changes permanently.

* To return to the latest copy of your code run the command `git checkout master`. This returns you to the "master" branch, and the latest commit.

#### Revert

The `git revert -m` operation undoes changes made in a specific commit, creating a second commit which effectively performs the reverse of the original.

* Use `git log` to find a commit you wish to revert, then run `git revert [identifier]`.

#### Reset

To permanently undo changes you can use the `git reset` command. This effectively rewrites history by returning the entire repository, not just your working directory, to a previous point in the commit history. Any commits you make from now on will act as though previous commits were not made. You should be careful when using `git reset`, especially if you have already published changes made.

To reset your repository to an earlier point, run `git log` to find a commit identifier, then run `git reset --hard [identifier]`.

## Introducing Bitbucket

[Bitbucket](https://bitbucket.org/) is our recommended external Git host, since they provide a free academic tier which gives you unlimited public and private Git repositories. For the rest of this workshop you will need a Bitbucket account, and we recommend that you continue using Bitbucket to keep your code safe in future.

* Visit [bitbucket.org/account/signup](https://bitbucket.org/account/signup/) to register for your free account if you haven't already. Choose an "individual" account, and be sure to use your lincoln.ac.uk email address. If you want to then you can change your email later, but you need to use your University address to earn the free academic account.
* Confirm your email address.

At the beginning of this workshop you were encouraged to note observations and changes you would make to this workshop to help yourself or others in future. You will now make a copy of the original of this workshop held on Bitbucket, make your changes, and contribute them back. The processes involved are called forking, cloning, and pull requests.

### Forking

Forking is the action of taking an existing repository and making a copy of it for your own purposes. A common example is in Open Source software, where people will fork a project to make their own changes even if they don't have permission to write to the original repository.

Forking isn't an inherent part of Git, but instead is commonly implemented by external Git hosts such as Bitbucket or GitHub. Forking is made possible (and easy) by Git being a distributed source control system, where any person with access to a copy of the code can choose to take development in their own direction.

* Visit [bitbucket.org/jacksonj04/introduction-to-git-bitbucket-workshop](https://bitbucket.org/jacksonj04/introduction-to-git-bitbucket-workshop) and click the "fork" button.
* You can change any of the information in the form if you want to, but if you're happy with the defaults just click the "Fork repository" button.
* Wait a moment while the repository is forked.

### Cloning

Now you have forked the repository you have your own copy of this workshop, with which you can do whatever you like. To make changes, however, you must have a copy of the code on your local computer. This is achieved using the `git clone` command.

* Make a new directory on your local computer, outside of your previous repository.
* If you are using Windows, right click the directory and select "Git Bash". OS X and Linux should open a Terminal and `cd` into the directory.
* Within the Bitbucket website, whilst looking at your new fork, click the "Clone" button.
* Choose "HTTPS" from the dropdown (It may say "SSH" to begin with).
* Copy and paste the entire contents of the text box. It should look something like `git clone https://jacksonj04@bitbucket.org/jacksonj04/introduction-to-git-bitbucket-workshop.git`
* Run the command. If you chose to make your repository private when forking then you will be asked for your Bitbucket password.

This will take a copy of the code from Bitbucket and store it locally, automatically creating the necessary remote. In this example your code will be created in a new folder called `introduction-to-git-bitbucket-workshop`, but this may very based on what you named your fork.

### Make Your Changes

There are two files in the repository -- Workshop.md and README.md. Both these files are in the Markdown text format, which can be changed with any text editor.

* Make your changes to the workshop in Workshop.md. If you need help with the Markdown syntax, check Useful Resources at the end of this document.
* Add your name (email address optional, remember that this is publicly visible) to the list of contributors in README.md.
* Commit your changes to your repository.

### Pushing Changes

Since you are working with a remote repository configured, you can push your changes back to Bitbucket. The command to do this is `git push`. Since you have not yet configured SSH keys, and you are using the HTTPS clone URL, you will be asked for your Bitbucket username when you run the command.

* Use `git push` to send your changes back to Bitbucket.

* When pushing changes, you will be asked to insert your password. This is the same as your Bitbucket password. Please don't despair if no text appears to be  entered into the field, believe me it is.

If you look at your fork on Bitbucket and refresh the page you should now see your changes appear on the website.

### Pull Request

A "pull request" is a request for somebody else to accept a set of changes you have made to the code. In this case it is asking for your changes to be accepted to this workshop, but it's commonly used by contributors to open source projects to allow vetting of new code and internally within companies to allow for code changes to be explicitly approved for inclusion in a shipping version of a product.

The exact meaning of "pull request" varies depending on if you are using an external host or not, but in this case we will assume your are wanting to make a Bitbucket pull request. The process for GitHub, the other most popular Git host, is almost identical. It is unlikely you will ever have to make a native Git pull request from the command line.

* Click the "Pull Request" button on your fork of the repository.
* Change the details in the form as necessary. You can choose the source -- this will be your fork -- and the destination -- which will be the original copy -- of the pull request.
* Leave a useful message for your pull request, detailing what has been changed.
* Click the "Create pull request" button.

That's it! Your code has now been added to a list of pull requests to be reviewed. You can see all the pending pull requests, as well as previously accepted or declined requests, at [bitbucket.org/jacksonj04/introduction-to-git-bitbucket-workshop/pull-requests](https://bitbucket.org/jacksonj04/introduction-to-git-bitbucket-workshop/pull-requests).

If your contribution is acceptable then it will be merged into this workshop's source, made permanently available and will be used in future sessions of this workshop.

## Your Own Code

This section of the workshop is deliberately left quite sparse. If you think it needs more detail please make some more changes in your fork of the repository and send in a pull request!

**For storing your own code it is likely that you will want a private repository rather than a publicly visible one, especially if the code is being used for assessment.**

* Create a new repository for some of your own code -- your group project work is ideal since this gives your entire group an opportunity to collaborate using source control. You can create a new Bitbucket repository at [bitbucket.org/repo/create](https://bitbucket.org/repo/create).

Once you have created a repository you should either `git clone` it locally (if you're starting from scratch) or use `git init` and `git remote add` to move some existing code to Bitbucket.

* Create at least one new branch of code using `git branch`, check it out using `git checkout` and make some changes.
* Merge your new branch back into `master` using `git merge`.
* `git push` your new code changes back into Bitbucket.

### Bring Your Friends

If you're working on code collaboratively, such as a group project, you can invite other members of your group to work on the same repository. This will give them the ability to `git push` code directly to the repository without using pull requests. You can add team members using the "Invite" link whilst viewing your repository on Bitbucket.

If you have been invited to a repository by someone else then you should use `git clone` to bring the latest copy of the code to your local machine.

## Useful Resources

* If you're stuck at any point, open Git Bash or a Terminal window and type `git help` or `git help [command]` to read the instructions.
* If you want to do some more reading on Git, including more in-depth information on techniques covered in this lecture, branching techniques, more uses of merging and loads of other stuff the entirety of Pro Git is available online for free at [git-scm.com/book](http://git-scm.com/book).
* Stack Overflow is full of questions and answers about Git, which you can find (or ask your own) at [stackoverflow.com/questions/tagged/git](http://stackoverflow.com/questions/tagged/git).
* This workshop document is written using Markdown. More information on the syntax is available at [daringfireball.net/projects/markdown](http://daringfireball.net/projects/markdown/).